package Test10;

import java.util.Scanner;

public class Main {
	public static void urutkanHuruf(String n) {
		char[] vokal = {'a','i','u','e','o'};
		String a = n.toLowerCase();
		char[] b = a.toCharArray();
		char[] temp1 = new char[b.length];
		char[] temp2 = new char[b.length];
		
		int x = 0;
		int y = 0;
		for (int i = 0; i < b.length; i++) {
			for (int j = 0; j < vokal.length; j++) {
				if(b[i] == vokal[j]) {
					temp1[x] = b[i];
					x = x + 1;
				} else {
					temp2[y] = b[i];
					y = y + 1;
				}
			}
		}
		
		System.out.println("Vokal : " + temp1);
		System.out.print("Konsonan : " + temp2);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myObj = new Scanner(System.in);
		System.out.print("string : ");
		String input1 = myObj.nextLine();
		
		urutkanHuruf(input1);
	}

}
