package Test6;

import java.util.Scanner;

public class Main {
	
	public static void angka(int n) {	
		for (int i = 0; i < n; i++) {
			if (i == 0) {
				continue;
			}
			
			if (i % 3 == 0) {
				if (i % 2 == 0) {
					System.out.println(i);
				}
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner myObj = new Scanner(System.in);
		System.out.print("Limit : ");
		int input1 = myObj.nextInt();
		
		angka(input1);

	}

}
